package com.boiko.fybertesttask;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.hamcrest.core.IsNot;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityEspressoTest {

  private static final String DEFAULT_APP_ID = "2070";
  private static final String DEFAULT_API_KEY = "1c915e3b5d42d05136185030892fbb846c278927";
  private static final String DEFAULT_PUB0 = "pub0";
  private static final String DEFAULT_UID = "spiderman";
  @Rule
  public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

  /**
   * GIVEN started MainActivity
   * WHEN I pressed Defaults button
   * Then I expect that all fields will be filled with valid data.
   */
  @Test
  public void testCheckDefaultsBtn() {
    onView(ViewMatchers.withId(R.id.btnSetDefaults)).perform(ViewActions.click());
    onView(ViewMatchers.withId(R.id.edtAppId)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_APP_ID)));
    onView(ViewMatchers.withId(R.id.edtPub0)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_PUB0)));
    onView(ViewMatchers.withId(R.id.edtApiKey)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_API_KEY)));
    onView(ViewMatchers.withId(R.id.edtUserId)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_UID)));
  }

  /**
   * GIVEN started MainActivity
   * WHEN I pressed Defaults and Request btns
   * Then I expect that new data will be achieved.
   */
  @Test
  public void testCheckRunWithDefaultsData() {
    onView(ViewMatchers.withId(R.id.btnSetDefaults)).perform(ViewActions.click());
    onView(ViewMatchers.withId(R.id.edtAppId)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_APP_ID)));
    onView(ViewMatchers.withId(R.id.edtPub0)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_PUB0)));
    onView(ViewMatchers.withId(R.id.edtApiKey)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_API_KEY)));
    onView(ViewMatchers.withId(R.id.edtUserId)).check(ViewAssertions.matches(ViewMatchers.withText(DEFAULT_UID)));

    onView(ViewMatchers.withId(R.id.btnRequest)).perform(ViewActions.click());

    onView(ViewMatchers.withId(R.id.empty)).check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())));
  }

  /**
   * GIVEN user did not fill API key
   * WHEN user presses Request button
   * THEN I expect that request will not be processed and a toast message will appear.
   */
  @Test
  public void testEmptyApiKeyInput() {
    onView(withId(R.id.edtAppId)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtUserId)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtPub0)).perform(ViewActions.typeText("sometext"));

    onView(ViewMatchers.withId(R.id.btnRequest)).perform(ViewActions.click());
    onView(ViewMatchers.withText(R.string.msg_emtpy_api_key)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView()))))
        .check(matches(isDisplayed()));
  }

  /**
   * GIVEN user did not fill AppID key
   * WHEN user presses Request button
   * THEN I expect that request will not be processed and a toast message will appear.
   */
  @Test
  public void testEmptyAppIdInput() {
    onView(withId(R.id.edtApiKey)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtUserId)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtPub0)).perform(ViewActions.typeText("sometext"));

    onView(ViewMatchers.withId(R.id.btnRequest)).perform(ViewActions.click());
    onView(ViewMatchers.withText(R.string.msg_emtpy_app_id)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView()))))
        .check(matches(isDisplayed()));
  }

  /**
   * GIVEN user did not fill UserID key
   * WHEN user presses Request button
   * THEN I expect that request will not be processed and a toast message will appear.
   */
  @Test
  public void testEmptyUserIdInput() {
    onView(withId(R.id.edtApiKey)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtAppId)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtPub0)).perform(ViewActions.typeText("sometext"));

    onView(ViewMatchers.withId(R.id.btnRequest)).perform(ViewActions.click());
    onView(ViewMatchers.withText(R.string.msg_emtpy_user_id)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView()))))
        .check(matches(isDisplayed()));
  }

  /**
   * GIVEN user did not fill Pub0 key
   * WHEN user presses Request button
   * THEN I expect that request will not be processed and a toast message will appear.
   */
  @Test
  public void testEmptyPub0Input() {
    onView(withId(R.id.edtApiKey)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtUserId)).perform(ViewActions.typeText("sometext"));
    onView(withId(R.id.edtAppId)).perform(ViewActions.typeText("sometext"));

    onView(ViewMatchers.withId(R.id.btnRequest)).perform(ViewActions.click());
    onView(ViewMatchers.withText(R.string.msg_emtpy_pub_0)).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView()))))
        .check(matches(isDisplayed()));
  }
}