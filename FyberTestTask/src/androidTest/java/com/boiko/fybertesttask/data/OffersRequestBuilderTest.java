package com.boiko.fybertesttask.data;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Unittest for {@link OffersRequestBuilder} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class OffersRequestBuilderTest {
  /**
   * GIVEN new {@link OffersRequestBuilder} instance
   * WHEN I set up all needed parameters and create new {@link OffersRequest} instance
   * THEN I expect that all fields will set up correctly.
   */
  @Test
  public void testCreateOfferRequest() throws UnsupportedEncodingException, NoSuchAlgorithmException {
    final String appIdExpected = "AppId";
    final String apiKeyExpected = "ApiKey";
    final String deviceIdExpected = "deviceId";
    final String gaidExpected = "Gaid";
    final boolean gaidEnabledExpected = true;
    final String pub0Expected = "Pub0";
    final String ipAddressExpected = "IpAddress";
    final String offerTypesExpected = "OfferTypes";
    final int timestampExpected = 34234234;
    final String psTimestampExpected = "PsTimestamp";
    final String userIdExpected = "UserId";

    final OffersRequestBuilder builder = new OffersRequestBuilder();
    builder.setAppId(appIdExpected);
    builder.setApiKey(apiKeyExpected);
    builder.setDeviceId(deviceIdExpected);
    builder.setGaid(gaidExpected);
    builder.setGaidEnabled(gaidEnabledExpected);
    builder.setPub0(pub0Expected);
    builder.setIpAddress(ipAddressExpected);
    builder.setUserId(userIdExpected);
    builder.setOfferTypes(offerTypesExpected);
    builder.setTimestamp(timestampExpected);
    builder.setPsTimestamp(psTimestampExpected);
    final OffersRequest offerRequest = builder.createOffersRequest();

    MatcherAssert.assertThat("AppId is not equal", appIdExpected.equals(offerRequest.getAppId()));
    MatcherAssert.assertThat("deviceId is not equal", deviceIdExpected.equals(offerRequest.getDeviceId()));
    MatcherAssert.assertThat("gaid is not equal", gaidExpected.equals(offerRequest.getGaid()));
    MatcherAssert.assertThat("gaidEnabled is not equal", gaidEnabledExpected == offerRequest.isGaidEnabled());
    MatcherAssert.assertThat("pub0 is not equal", pub0Expected.equals(offerRequest.getPub0()));
    MatcherAssert.assertThat("ipAddress is not equal", ipAddressExpected.equals(offerRequest.getIpAddress()));
    MatcherAssert.assertThat("offerTypes is not equal", offerTypesExpected.equals(offerRequest.getOfferTypes()));
    MatcherAssert.assertThat("timestam is not equal", timestampExpected == offerRequest.getTimestamp());
    MatcherAssert.assertThat("psTimestamp is not equal", psTimestampExpected.equals(offerRequest.getUnixTimestamp()));
    MatcherAssert.assertThat("userId is not equal", userIdExpected.equals(offerRequest.getUserId()));
  }
}