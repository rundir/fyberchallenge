package com.boiko.fybertesttask.data;

import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Unittest for {@link ApiHashGenerator} class.
 */
public class ApiHashGeneratorTest {
  /**
   * GIVEN an {@link ApiHashGenerator} instance
   * WHEN I use {@link ApiHashGenerator#concatenatedParametersToHash(List, String)} to create concatenated parameters
   * THEN I expect that it will be executed correctly.
   */
  @Test
  public void testConcatenatedParametersToHash() throws Exception {
    final ApiHashGenerator apiHashGenerator = new ApiHashGenerator();
    final List<String> parameters = Arrays.asList("A", "C", "V");
    final String apikey = "apikey";
    final String result = apiHashGenerator.concatenatedParametersToHash(parameters, apikey);
    MatcherAssert.assertThat("Wrong concatenated parameters: " + result, "A&C&V&apikey".equals(result));
  }
}