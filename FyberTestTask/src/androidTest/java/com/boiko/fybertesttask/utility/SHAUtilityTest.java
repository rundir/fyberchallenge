package com.boiko.fybertesttask.utility;

import junit.framework.TestCase;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Unittest for {@link SHAUtility} class.
 */
@RunWith(MockitoJUnitRunner.class)
public class SHAUtilityTest {
  /**
   * GIVEN text which hash needs to be calculated
   * WHEN I use {@link SHAUtility#SHA1(String)} method
   * THEN I receive valid data.
   */
  @Test
  public void testSHA1() throws Exception {
    final String sample = "Simple text to use";
    final String expectedHash = "372ed82a39f06d276a0fcca231f5e33b13e640eb";
    MatcherAssert.assertThat("", expectedHash.equals(SHAUtility.SHA1(sample)));
  }
}