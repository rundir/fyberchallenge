package com.boiko.fybertesttask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.boiko.fybertesttask.data.OffersRequest;
import com.boiko.fybertesttask.data.OffersRequestBuilder;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Fragment that provides form to setup a request parameters.
 */
public class RequestSetupFragment extends Fragment {
  private static final String DEFAULT_APP_ID = "2070";
  private static final String DEFAULT_API_KEY = "1c915e3b5d42d05136185030892fbb846c278927";
  private static final String DEFAULT_PUB0 = "pub0";
  private static final String DEFAULT_UID = "spiderman";

  private EditText edtAppId;
  private EditText edtUserId;
  private EditText edtApiKey;
  private EditText edtPub0;

  public interface RequestSetupListener {
    void onRequest(final OffersRequest offersRequest);
  }

  private RequestSetupListener requestSetupListener;

  public RequestSetupFragment() {
  }

  @Override
  public void onAttach(final Context context) {
    super.onAttach(context);
    if (context instanceof RequestSetupListener) {
      requestSetupListener = (RequestSetupListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
    }
  }

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_request_setup, container, false);
  }

  @Override
  public void onViewCreated(final View view, final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    edtAppId = (EditText) view.findViewById(R.id.edtAppId);
    edtUserId = (EditText) view.findViewById(R.id.edtUserId);
    edtApiKey = (EditText) view.findViewById(R.id.edtApiKey);
    edtPub0 = (EditText) view.findViewById(R.id.edtPub0);

    final Button btnRequest = (Button) view.findViewById(R.id.btnRequest);
    final Button btnDefault = (Button) view.findViewById(R.id.btnSetDefaults);

    btnDefault.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        edtAppId.setText(DEFAULT_APP_ID);
        edtUserId.setText(DEFAULT_UID);
        edtApiKey.setText(DEFAULT_API_KEY);
        edtPub0.setText(DEFAULT_PUB0);
      }
    });

    btnRequest.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        if (isValidFields()) {
          new PrepareRequestTask(edtAppId.getText().toString(), edtUserId.getText().toString(),
              edtApiKey.getText().toString(), edtPub0.getText().toString()).execute();
        }
      }
    });
  }

  private boolean isValidFields() {
    boolean result = true;
    if (TextUtils.isEmpty(edtApiKey.getText().toString())) {
      Toast.makeText(getContext(), R.string.msg_emtpy_api_key, Toast.LENGTH_SHORT).show();
      result = false;
    } else if (TextUtils.isEmpty(edtAppId.getText().toString())) {
      Toast.makeText(getContext(), R.string.msg_emtpy_app_id, Toast.LENGTH_SHORT).show();
      result = false;
    } else if (TextUtils.isEmpty(edtPub0.getText().toString())) {
      Toast.makeText(getContext(), R.string.msg_emtpy_pub_0, Toast.LENGTH_SHORT).show();
      result = false;
    } else if (TextUtils.isEmpty(edtUserId.getText().toString())) {
      Toast.makeText(getContext(), R.string.msg_emtpy_user_id, Toast.LENGTH_SHORT).show();
      result = false;
    }
    return result;
  }

  private class PrepareRequestTask extends AsyncTask<Void, Void, OffersRequestBuilder> {
    private final String appId;
    private final String uid;
    private final String apiKey;
    private final String pub0;
    private ProgressDialog pd;

    public PrepareRequestTask(final String appId, final String uid, final String apiKey, final String pub0) {
      this.appId = appId;
      this.uid = uid;
      this.apiKey = apiKey;
      this.pub0 = pub0;
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      pd = ProgressDialog.show(getContext(), getResources().getString(R.string.title_pd_verification),
          getResources().getString(R.string.msg_pd_checking));
    }

    @Override
    protected OffersRequestBuilder doInBackground(final Void... params) {
        AdvertisingIdClient.Info advertisingIdInfo = null;
        try {
          advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(getContext());
        } catch (final IOException | GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e) {
          e.printStackTrace();
        }
        assert advertisingIdInfo != null;

        final OffersRequestBuilder builder = new OffersRequestBuilder();
        builder.setAppId(appId)
            .setUserId(uid)
            .setPub0(pub0)
            .setApiKey(apiKey)
            .setTimestamp(System.currentTimeMillis() / 1000)
            .setDeviceId(advertisingIdInfo.getId())
            .setIpAddress("109.235.143.113");

      return builder;
    }

    @Override
    protected void onPostExecute(final OffersRequestBuilder builder) {
      super.onPostExecute(builder);
      pd.dismiss();
      if (requestSetupListener != null) {
        try {
          requestSetupListener.onRequest(builder.createOffersRequest());
        } catch (final UnsupportedEncodingException e) {
          e.printStackTrace();
        } catch (final NoSuchAlgorithmException e) {
          e.printStackTrace();
        }

      }
    }
  }
}
