package com.boiko.fybertesttask;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.boiko.fybertesttask.data.OffersRequest;
import com.boiko.fybertesttask.data.OffersResponseBody;

/**
 * An Activity that manages a comunication request between fragments.
 */
public class MainActivity extends AppCompatActivity implements RequestSetupFragment.RequestSetupListener, OffersFragment.OnListFragmentInteractionListener {
  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportFragmentManager().beginTransaction().add(R.id.body, new RequestSetupFragment(), null).commit();
  }

  @Override
  public void onRequest(final OffersRequest offersRequest) {
    getSupportFragmentManager().beginTransaction().replace(R.id.body, OffersFragment.newInstance(offersRequest), null).addToBackStack(null).commit();
  }

  @Override
  public void onListFragmentInteraction(final OffersResponseBody.Offer item) {
  }
}
