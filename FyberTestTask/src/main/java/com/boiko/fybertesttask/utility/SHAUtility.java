package com.boiko.fybertesttask.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utility class that contains method to work with SHA.
 */
public class SHAUtility {
  /**
   * Calculates hash of the given text by SHA1 algorithm.
   *
   * @param text the given text to calculate.
   * @return hash of the given text.
   * @throws NoSuchAlgorithmException     if the SHA-1 algorithm is not available
   * @throws UnsupportedEncodingException if the charset is not supported.
   */
  public static String SHA1(final String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    final MessageDigest md = MessageDigest.getInstance("SHA-1");
    final byte[] bytes = text.getBytes("UTF-8");
    md.update(bytes, 0, bytes.length);
    final byte[] sha1hash = md.digest();
    return convertToHex(sha1hash);
  }

  private static String convertToHex(final byte[] data) {
    final StringBuilder buf = new StringBuilder();
    for (final byte b : data) {
      int halfbyte = b >>> 4 & 0x0F;
      int two_halfs = 0;
      do {
        buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
        halfbyte = b & 0x0F;
      } while (two_halfs++ < 1);
    }
    return buf.toString();
  }
}