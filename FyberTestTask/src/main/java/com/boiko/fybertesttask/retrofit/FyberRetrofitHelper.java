package com.boiko.fybertesttask.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Helper class that creates {@link Retrofit} instance.
 */
public class FyberRetrofitHelper {
  private static final String BASE_URL = "http://api.fyber.com/feed/v1/";
  private static final FyberRetrofitHelper fyberRetrofitHelperInstance = new FyberRetrofitHelper();
  private final Retrofit retrofit;

  private FyberRetrofitHelper() {
    final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    final OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    retrofit = new Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build();
  }

  public static FyberRetrofitHelper getInstance() {
    return fyberRetrofitHelperInstance;
  }

  public FyberService create() {
    return retrofit.create(FyberService.class);
  }
}
