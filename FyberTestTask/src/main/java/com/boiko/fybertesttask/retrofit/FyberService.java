package com.boiko.fybertesttask.retrofit;

import com.boiko.fybertesttask.data.OffersApiParameters;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * API interface.
 */
public interface FyberService {
  @GET("offers.json")
  Call<ResponseBody> getOffers(@Query(OffersApiParameters.APP_ID) String appID,
                               @Query(OffersApiParameters.DEVICE_ID) String device_id,
                               @Query(OffersApiParameters.FORMAT) String json,
                               @Query(OffersApiParameters.IP) String ip,
                               @Query(OffersApiParameters.LOCALE) String locale,
                               @Query(OffersApiParameters.OFFER_TYPES) String offer_types,
                               @Query(OffersApiParameters.TIMESTAMP) long timestamp,
                               @Query(OffersApiParameters.PUB0) String pub0,
                               @Query(OffersApiParameters.UID) String uid,
                               @Query(OffersApiParameters.HASHKEY) String hash);
}