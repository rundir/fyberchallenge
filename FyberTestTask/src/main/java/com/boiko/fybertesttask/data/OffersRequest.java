package com.boiko.fybertesttask.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Data class that store all parameters needed for offers request.
 */
public class OffersRequest implements Parcelable {
  private static final String TAG = OffersRequest.class.getSimpleName();

  private static final String DEFAULT_FORMAT = "json";
  private static final String DEFAULT_LOCALE = "de";
  private static final String DEFAULT_OFFER_TYPES = "112";

  private final String appId;
  private final String userId;
  private final String ipAddress;
  private final String deviceId;
  private final long timestamp;
  private final String pub0;
  private final String unixTimestamp;
  private final String offerTypes;
  private final String gaid;
  private final boolean gaidEnabled;
  private final String hashkey;
  private final String apiKey;

  OffersRequest(final HashGenerator hashGenerator, final String appId, final String userId, final String ipAddress, final String deviceId,
                final long timestamp, final String pub0, final String unixTimestamp, final String offerTypes,
                final String gaid, final boolean gaidEnabled, final String apiKey) throws UnsupportedEncodingException, NoSuchAlgorithmException {
    this.appId = appId;
    this.userId = userId;
    this.ipAddress = ipAddress;
    this.deviceId = deviceId;
    this.timestamp = timestamp;
    this.pub0 = pub0;
    this.unixTimestamp = unixTimestamp;
    this.offerTypes = offerTypes;
    this.gaid = gaid;
    this.gaidEnabled = gaidEnabled;
    this.apiKey = apiKey;

    this.hashkey = hashGenerator.generateHash(hashGenerator.concatenatedParametersToHash(createParamsValuesList(), apiKey));
  }

  public String getAppId() {
    return appId;
  }

  public String getUserId() {
    return userId;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public String getPub0() {
    return pub0;
  }

  public String getUnixTimestamp() {
    return unixTimestamp;
  }

  public String getOfferTypes() {
    return offerTypes;
  }

  public String getGaid() {
    return gaid;
  }

  public boolean isGaidEnabled() {
    return gaidEnabled;
  }

  public String getDefaultOfferTypes() {
    return DEFAULT_OFFER_TYPES;
  }

  public String getFORMAT() {
    return DEFAULT_FORMAT;
  }

  public String getDefaultLocale() {
    return DEFAULT_LOCALE;
  }

  public String getApiKey() {
    return apiKey;
  }

  public String getHash() {
    return hashkey;
  }

  private List<String> createParamsValuesList() {
    final List<String> paramsAndValues = new ArrayList<>();
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.APP_ID, appId));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.FORMAT, DEFAULT_FORMAT));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.UID, userId));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.LOCALE, DEFAULT_LOCALE));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.DEVICE_ID, deviceId));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.PUB0, pub0));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.IP, ipAddress));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.OFFER_TYPES, DEFAULT_OFFER_TYPES));
    paramsAndValues.add(concateParamAndValue(OffersApiParameters.TIMESTAMP, timestamp));

    return paramsAndValues;
  }

  private String concateParamAndValue(final String key, final Object value) {
    return key + "=" + value;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(final Parcel dest, final int flags) {
    dest.writeString(this.appId);
    dest.writeString(this.userId);
    dest.writeString(this.ipAddress);
    dest.writeString(this.deviceId);
    dest.writeLong(this.timestamp);
    dest.writeString(this.pub0);
    dest.writeString(this.unixTimestamp);
    dest.writeString(this.offerTypes);
    dest.writeString(this.gaid);
    dest.writeByte(gaidEnabled ? (byte) 1 : (byte) 0);
    dest.writeString(this.hashkey);
    dest.writeString(apiKey);
  }

  protected OffersRequest(final Parcel in) {
    this.appId = in.readString();
    this.userId = in.readString();
    this.ipAddress = in.readString();
    this.deviceId = in.readString();
    this.timestamp = in.readLong();
    this.pub0 = in.readString();
    this.unixTimestamp = in.readString();
    this.offerTypes = in.readString();
    this.gaid = in.readString();
    this.gaidEnabled = in.readByte() != 0;
    this.hashkey = in.readString();
    apiKey = in.readString();
  }

  public static final Creator<OffersRequest> CREATOR = new Creator<OffersRequest>() {
    public OffersRequest createFromParcel(final Parcel source) {
      return new OffersRequest(source);
    }

    public OffersRequest[] newArray(final int size) {
      return new OffersRequest[size];
    }
  };
}
