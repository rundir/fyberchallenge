package com.boiko.fybertesttask.data;

/**
 * Created by rundir on 5-2-16.
 */
public class OffersApiParameters {
  public static final String APP_ID = "appid";
  public static final String UID = "uid";
  public static final String IP = "ip";
  public static final String LOCALE = "locale";
  public static final String DEVICE_ID = "device_id";
  public static final String PUB0 = "pub0";
  public static final String TIMESTAMP = "timestamp";
  public static final String HASHKEY = "hashkey";
  /** not used. */
  public static final String PS_TIME = "ps_time";
  public static final String GOOGLE_AD_ID = "google_ad_id";
  public static final String GOOGLE_AD_ID_LIMITED_TRACKING_ENABLED = "google_ad_id_limited_tracking_enabled";
  public static final String FORMAT = "format";
  public static final String OFFER_TYPES = "offer_types";
  public static final String OS_VERSION = "os_version";
}
