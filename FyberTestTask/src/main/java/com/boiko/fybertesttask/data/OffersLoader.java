package com.boiko.fybertesttask.data;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.boiko.fybertesttask.retrofit.FyberRetrofitHelper;
import com.boiko.fybertesttask.retrofit.FyberService;
import com.boiko.fybertesttask.utility.SHAUtility;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Loader that retrieves offers through API.
 */
public class OffersLoader extends AsyncTaskLoader<OffersResponse> {
  private final OffersRequest offersRequest;

  public OffersLoader(final Context context, final OffersRequest offersRequest) {
    super(context);
    this.offersRequest = offersRequest;
    onContentChanged();
  }

  @Override
  public void onStartLoading() {
    if (takeContentChanged())
      forceLoad();
  }

  @Override
  public OffersResponse loadInBackground() {
    final OffersResponseBuilder builder = new OffersResponseBuilder();
    final FyberService fyberService = FyberRetrofitHelper.getInstance().create();

    final Call<ResponseBody> offersCallBody = fyberService.getOffers(offersRequest.getAppId(),
        offersRequest.getDeviceId(),
        offersRequest.getFORMAT(),
        offersRequest.getIpAddress(),
        offersRequest.getDefaultLocale(),
        offersRequest.getDefaultOfferTypes(),
        offersRequest.getTimestamp(),
        offersRequest.getPub0(),
        offersRequest.getUserId(),
        offersRequest.getHash());

    Response<ResponseBody> data = null;
    try {
      data = offersCallBody.execute();
    } catch (final IOException e) {
      builder.setCode(OffersResponse.ERROR_CONNECTION, e);
    }
    try {
      if (data != null && data.isSuccess()) {
        final String responseHash = data.headers().get("X-Sponsorpay-Response-Signature");
        final String body = data.body().string();

        if (SHAUtility.SHA1(body + offersRequest.getApiKey()).equals(responseHash)) {
          final OffersResponseBody offersResponseBody = new Gson().fromJson(body, OffersResponseBody.class);
          builder.setResponseHash(responseHash).setCode(data.code()).setResponseBody(data.errorBody()).setOffersResponseBody(offersResponseBody);
        } else {
          builder.setCode(OffersResponse.ERROR_BAD_SIGN);
        }
      }
    } catch (final IOException e) {
      builder.setCode(OffersResponse.ERROR_GSON, e);
    } catch (final NoSuchAlgorithmException e) {
      builder.setCode(OffersResponse.ERROR_SHA1, e);
    }
    return builder.createOffersResponse();
  }

  @Override
  public void onStopLoading() {
    cancelLoad();
  }
}
