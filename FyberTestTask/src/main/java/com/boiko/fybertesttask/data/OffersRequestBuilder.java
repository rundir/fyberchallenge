package com.boiko.fybertesttask.data;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Builder class that builds {@link OffersRequest}.
 */
public class OffersRequestBuilder {
  private String appId;
  private String userId;
  private String ipAddress;
  private String deviceId;
  private long timestamp;
  private String pub0;
  private String unixTimestamp;
  private String offerTypes;
  private String gaid;
  private boolean gaidEnabled;
  private String apiKey;

  public OffersRequestBuilder setAppId(final String appId) {
    this.appId = appId;
    return this;
  }

  public OffersRequestBuilder setUserId(final String userId) {
    this.userId = userId;
    return this;
  }

  public OffersRequestBuilder setIpAddress(final String ipAddress) {
    this.ipAddress = ipAddress;
    return this;
  }

  public OffersRequestBuilder setDeviceId(final String deviceId) {
    this.deviceId = deviceId;
    return this;
  }

  public OffersRequestBuilder setTimestamp(final long timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  public OffersRequestBuilder setPub0(final String pub0) {
    this.pub0 = pub0;
    return this;
  }

  public OffersRequestBuilder setPsTimestamp(final String unixTimestamp) {
    this.unixTimestamp = unixTimestamp;
    return this;
  }

  public OffersRequestBuilder setOfferTypes(final String offerTypes) {
    this.offerTypes = offerTypes;
    return this;
  }

  public OffersRequestBuilder setGaid(final String gaid) {
    this.gaid = gaid;
    return this;
  }

  public OffersRequestBuilder setGaidEnabled(final boolean gaidEnabled) {
    this.gaidEnabled = gaidEnabled;
    return this;
  }

  public OffersRequestBuilder setApiKey(final String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  public OffersRequest createOffersRequest() throws UnsupportedEncodingException, NoSuchAlgorithmException {
    return createOffersRequest(new ApiHashGenerator());
  }

  public OffersRequest createOffersRequest(final HashGenerator hashGenerator) throws UnsupportedEncodingException, NoSuchAlgorithmException {
    return new OffersRequest(hashGenerator, appId, userId, ipAddress, deviceId, timestamp, pub0, unixTimestamp, offerTypes, gaid, gaidEnabled, apiKey);
  }
}