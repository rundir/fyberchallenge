package com.boiko.fybertesttask.data;

/**
 * Mapper class that represents API response for offers request.
 */
public class OffersResponseBody {
  private String code;
  private String message;
  private int count;
  private int pages;
  private OffersInformation information;
  private Offer[] offers;

  public Offer[] getOffers() {
    return offers;
  }

  public static class OffersInformation {
    private String app_name;
    private int appid;
    private String virtual_currency;
    private boolean virtual_currency_sale_enabled;
    private String country;
    private String language;
    private String support_url;
  }

  public static class Offer {
    private String link;
    private String title;
    private int offer_id;
    private String teaser;
    private String required_actions;
    private String store_id;
    private Thumbnail thumbnail;
    private OfferType[] offer_types;
    private int payout;
    private TimeToPayout time_to_payout;

    public String getLink() {
      return link;
    }

    public String getTitle() {
      return title;
    }

    public int getOffer_id() {
      return offer_id;
    }

    public String getTeaser() {
      return teaser;
    }

    public String getRequired_actions() {
      return required_actions;
    }

    public Thumbnail getThumbnail() {
      return thumbnail;
    }

    public OfferType[] getOffer_types() {
      return offer_types;
    }

    public int getPayout() {
      return payout;
    }

    public TimeToPayout getTime_to_payout() {
      return time_to_payout;
    }
  }

  public static class OfferType {
    private int offer_type_id;
    private String readable;
  }

  public static class Thumbnail {
    private String lowres;
    private String hires;

    public String getLowres() {
      return lowres;
    }

    public String getHires() {
      return hires;
    }
  }

  public static class TimeToPayout {
    private int amount;
    private String readable;
  }
}