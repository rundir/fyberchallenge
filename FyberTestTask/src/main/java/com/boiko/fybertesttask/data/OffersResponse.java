package com.boiko.fybertesttask.data;

import okhttp3.ResponseBody;

/**
 * Data class to store Offers API response data.
 */
public class OffersResponse {
  public final static int ERROR_CONNECTION = -1;
  public final static int ERROR_GSON = -2;
  public final static int ERROR_SHA1 = -3;
  public final static int ERROR_BAD_SIGN = -4;

  private final ResponseBody responseBody;
  private final String responseHash;
  private final int code;
  private final OffersResponseBody offersResponseBody;
  private final Exception exception;

  public OffersResponse(final String responseHash, final int code, final ResponseBody responseBody, final OffersResponseBody offersResponseBody, final Exception exception) {
    this.responseBody = responseBody;
    this.responseHash = responseHash;
    this.code = code;
    this.offersResponseBody = offersResponseBody;
    this.exception = exception;
  }

  public ResponseBody getResponseBody() {
    return responseBody;
  }

  public String getResponseHash() {
    return responseHash;
  }

  public int getCode() {
    return code;
  }

  public OffersResponseBody getOffersResponseBody() {
    return offersResponseBody;
  }

  public Exception getException() {
    return exception;
  }

  public boolean isSuccess() {
    return exception == null;
  }
}
