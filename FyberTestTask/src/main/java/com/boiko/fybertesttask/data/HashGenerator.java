package com.boiko.fybertesttask.data;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Api hash generator.
 */
public interface HashGenerator {
  /**
   * Concatenate the given parameters with the API key.
   *
   * @param paramsAndValues the API parameters.
   * @param apiKey          the API key
   * @return the concatenated string.
   */
  String concatenatedParametersToHash(List<String> paramsAndValues, String apiKey);

  /**
   * Generates HASH from the given string
   *
   * @param concatenatedParamsWithApiKey the given string to generate HASH.
   * @return the HASH of the given string.
   * @throws UnsupportedEncodingException
   * @throws NoSuchAlgorithmException
   */
  String generateHash(final String concatenatedParamsWithApiKey) throws UnsupportedEncodingException, NoSuchAlgorithmException;
}
