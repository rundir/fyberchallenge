package com.boiko.fybertesttask.data;

import android.text.TextUtils;

import com.boiko.fybertesttask.utility.SHAUtility;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;

/**
 * Implementations of {@link HashGenerator} regarding to API.
 */
public class ApiHashGenerator implements HashGenerator {
  @Override
  public String concatenatedParametersToHash(final List<String> paramsAndValues, final String apiKey) {
    Collections.sort(paramsAndValues);

    final String concatedParams = TextUtils.join("&", paramsAndValues);

    return concatedParams + "&" + apiKey;
  }

  @Override
  public String generateHash(final String concatenatedParamsWithApiKey) throws UnsupportedEncodingException, NoSuchAlgorithmException {
    return SHAUtility.SHA1(concatenatedParamsWithApiKey);
  }
}
