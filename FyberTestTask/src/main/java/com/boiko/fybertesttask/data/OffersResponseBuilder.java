package com.boiko.fybertesttask.data;

import okhttp3.ResponseBody;

public class OffersResponseBuilder {
  private String responseHash;
  private int code;
  private ResponseBody responseBody;
  private OffersResponseBody offersResponseBody;
  private Exception exception;

  public OffersResponseBuilder setResponseHash(final String responseHash) {
    this.responseHash = responseHash;
    return this;
  }

  public OffersResponseBuilder setCode(final int code) {
    this.code = code;
    return this;
  }

  public OffersResponseBuilder setCode(final int code, final Exception e) {
    this.code = code;
    this.exception = e;
    return this;
  }

  public OffersResponseBuilder setResponseBody(final ResponseBody responseBody) {
    this.responseBody = responseBody;
    return this;
  }

  public OffersResponseBuilder setOffersResponseBody(final OffersResponseBody offersResponseBody) {
    this.offersResponseBody = offersResponseBody;
    return this;
  }

  public OffersResponse createOffersResponse() {
    return new OffersResponse(responseHash, code, responseBody, offersResponseBody, exception);
  }
}