package com.boiko.fybertesttask;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.boiko.fybertesttask.data.OffersLoader;
import com.boiko.fybertesttask.data.OffersRequest;
import com.boiko.fybertesttask.data.OffersResponse;
import com.boiko.fybertesttask.data.OffersResponseBody;

import java.util.Arrays;

/**
 * A fragment representing a list of {@link OffersResponseBody.Offer}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class OffersFragment extends Fragment implements LoaderManager.LoaderCallbacks<OffersResponse> {
  private static final String ARG_COLUMN_COUNT = "column-count";
  private static final String ARG_REQUEST = "request_arguments";
  private static final int DEFAULT_COLUMN_COUNT = 1;

  private int columnCount;
  private OnListFragmentInteractionListener listener;
  private OffersRecyclerViewAdapter offerRecyclerViewAdapter;
  private OffersRequest offersRequest;
  private View emptyView;
  private View progress;
  private RecyclerView recyclerView;

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the
   * fragment (e.g. upon screen orientation changes).
   */
  public OffersFragment() {
  }

  public static OffersFragment newInstance(final OffersRequest offersRequest) {
    return newInstance(DEFAULT_COLUMN_COUNT, offersRequest);
  }

  /**
   * Constructs new OffersFragment instance with given parameters.
   *
   * @param columnCount   a number of column to display.
   * @param offersRequest an offer request to execute.
   * @return a new OffersFragment instance.
   */
  public static OffersFragment newInstance(final int columnCount, final OffersRequest offersRequest) {
    final OffersFragment fragment = new OffersFragment();
    final Bundle args = new Bundle();
    args.putInt(ARG_COLUMN_COUNT, columnCount);
    args.putParcelable(ARG_REQUEST, offersRequest);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (getArguments() != null) {
      columnCount = getArguments().getInt(ARG_COLUMN_COUNT);
      offersRequest = getArguments().getParcelable(ARG_REQUEST);
    }

    offerRecyclerViewAdapter = new OffersRecyclerViewAdapter(getContext(), null, listener);
  }

  @Override
  public void onActivityCreated(final Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    setListShown(false);
    getLoaderManager().initLoader(0, null, this);
  }

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                           final Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.fragment_offer_list, container, false);
    final Context context = view.getContext();
    recyclerView = (RecyclerView) view.findViewById(R.id.list);
    progress = view.findViewById(R.id.progress);
    emptyView = view.findViewById(R.id.empty);

    if (columnCount <= 1) {
      recyclerView.setLayoutManager(new LinearLayoutManager(context));
    } else {
      recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
    }
    recyclerView.setAdapter(offerRecyclerViewAdapter);
    return view;
  }


  @Override
  public void onAttach(final Context context) {
    super.onAttach(context);
    if (context instanceof OnListFragmentInteractionListener) {
      listener = (OnListFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    listener = null;
  }

  @Override
  public Loader<OffersResponse> onCreateLoader(final int id, final Bundle args) {
    return new OffersLoader(getContext(), offersRequest);
  }

  @Override
  public void onLoadFinished(final Loader<OffersResponse> loader, final OffersResponse data) {
    if (data.isSuccess()) {
      offerRecyclerViewAdapter.setOffers(Arrays.asList(data.getOffersResponseBody().getOffers()));
    } else {
      Toast.makeText(getContext(), "error= " + data.getCode(), Toast.LENGTH_SHORT).show();
    }
    setListShown(true);
  }

  @Override
  public void onLoaderReset(final Loader<OffersResponse> loader) {

  }

  public interface OnListFragmentInteractionListener {
    void onListFragmentInteraction(OffersResponseBody.Offer item);
  }

  private void setListShown(final boolean show) {
    if (show) {
      if (offerRecyclerViewAdapter.getItemCount() > 0) {
        recyclerView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
      } else {
        recyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
      }
    } else {
      recyclerView.setVisibility(View.GONE);
      emptyView.setVisibility(View.GONE);
      progress.setVisibility(View.VISIBLE);
    }
  }
}
