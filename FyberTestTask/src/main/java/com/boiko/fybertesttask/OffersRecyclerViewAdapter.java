package com.boiko.fybertesttask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.boiko.fybertesttask.OffersFragment.OnListFragmentInteractionListener;
import com.boiko.fybertesttask.data.OffersResponseBody;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * TODO: Replace the implementation with code for your data type.
 */
public class OffersRecyclerViewAdapter extends RecyclerView.Adapter<OffersRecyclerViewAdapter.ViewHolder> {

  private final Context context;
  private List<OffersResponseBody.Offer> offers;
  private final OnListFragmentInteractionListener onListFragmentInteractionListener;

  public OffersRecyclerViewAdapter(final Context context, final List<OffersResponseBody.Offer> offers,
                                   final OnListFragmentInteractionListener listener) {
    this.context = context;
    this.offers = offers;
    onListFragmentInteractionListener = listener;
  }

  public void setOffers(final List<OffersResponseBody.Offer> offers) {
    this.offers = offers;
    notifyDataSetChanged();
  }

  @Override
  public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
    final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_offer, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, final int position) {
    final OffersResponseBody.Offer offer = offers.get(position);
    holder.titleTextView.setText(offer.getTitle());
    holder.teaserTextView.setText(offer.getTeaser());
    holder.payoutTextView.setText(String.format("%d", offer.getPayout()));

    holder.view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        if (null != onListFragmentInteractionListener) {
          // Notify the active callbacks interface (the activity, if the
          // fragment is attached to one) that an offer has been selected.
          onListFragmentInteractionListener.onListFragmentInteraction(offer);
        }
      }
    });

    Glide.with(context.getApplicationContext())
        .load(offer.getThumbnail().getHires())
        .skipMemoryCache(true)
        .centerCrop()
        .into(holder.thumbImageView);
  }

  @Override
  public int getItemCount() {
    return (offers == null) ? 0 : offers.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final View view;
    public final TextView titleTextView;
    public final TextView teaserTextView;
    public final TextView payoutTextView;
    public final ImageView thumbImageView;

    public ViewHolder(final View view) {
      super(view);
      this.view = view;
      titleTextView = (TextView) view.findViewById(R.id.title);
      teaserTextView = (TextView) view.findViewById(R.id.teaser);
      payoutTextView = (TextView) view.findViewById(R.id.payout);
      thumbImageView = (ImageView) view.findViewById(R.id.thumb);
    }

    @Override
    public String toString() {
      return super.toString() + " '" + teaserTextView.getText() + "'";
    }
  }
}